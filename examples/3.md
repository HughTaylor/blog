Third post
2023-06-22
Public
Why bother with databases, or frameworks, or templates when you can just write a script that takes a bunch of text files as input and turns them into blog posts!
The simplest way to do this is to use a very simple file format, where the first line is the post title, the second the date, the third the status, and the rest of the lines contain the content of the post.
