# Publish a blog

A tool for quickly generating html pages for a blog from md files. Automatically links to the next and previous posts.

## Instructions

1. Create .md files in the "posts" folder.
2. Run "python publish.py"
3. html files will be output with the same names as the md files. They will be linked in alphabetical order.2. Run "python publish.py"

