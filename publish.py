import os
import markdown

# maintain a list of saved files. Every time we create a new file, we check if there was a previous one, link back to it. Then we edit the previous one and link forward to the current one.
list_of_posts = []
path = "./posts/"

def move_files():
    for post in list_of_posts:
        os.rename(post, "blog/" + post)

def add_closing_tags():
    for post in list_of_posts:
        out = open(post, "a")
        concat('./footer.txt', out)
        out.close()

def concat(infile, outfile):
    with open(infile) as infile:
        for line in infile:
            outfile.write(line)

def parse_md(file_name, file_path):
    with open(file_path) as f:
        output_file = (file_name.rsplit( ".", 1 )[ 0 ]) + '.html'
        title = f.readline().strip()
        date = f.readline().strip()
        status = f.readline().strip()
        content = '\n'.join(f.readlines())
        if status.lower() == "public":
            out = open(output_file, "w")
            concat('./header.txt', out)
            out.write("<h2>" + title + "</h2>")
            #out.write("<h3>" + date + "</h3>")
            out.write(markdown.markdown(content))
            out.write("<footer>")
            if len(list_of_posts):
                # create link to previous post
                out.write('<a href="' + list_of_posts[-1] + '">Previous post</a>')
                out.close()
                # open previous post, and create link to this post
                out = open(list_of_posts[-1], "a")
                out.write('<a href="' + output_file + '">Next post</a>')
                out.close()
            else:
                out.write("<span></span>")
                out.close()

            print(" complete")
            list_of_posts.append(output_file)


def walk_files():
    posts = []
    with os.scandir(path) as it:
        for entry in it:
            if entry.name.endswith(".md") and entry.is_file():
                # add all the md files to the "posts" list
                posts.append([entry.name, entry.path])

    # "posts" list needs to be sorted
    posts.sort()
    for post in posts:
        print("Processing " + post[0], end='')
        parse_md(post[0], post[1])

def main():
    walk_files()
    add_closing_tags()
    move_files()

if __name__ == "__main__":
    main()
